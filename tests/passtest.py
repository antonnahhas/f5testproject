import pytest
import urllib.request

url = "https://the-internet.herokuapp.com/context_menu"
file = urllib.request.urlopen(url)

for line in file:
    decode_line = line.decode("utf-8")
    assert decode_line == "Right-click in the box below to see one called 'the-internet'"